//Atribuição de variavel via desentruturação
let someArray = ['um', 'dois', 'tres'];
let [um, dois, tres] = someArray;
console.log(um, dois, tres);

//Desestruturando um Obj e atribuindo a variaveis de mesmo nome!
let me = {
    nome: 'Marcelo',
    sobrenome: 'Nassar',
}
let { nome, sobrenome  } = me;
console.log('meu nome é ' + nome + ' ' + sobrenome);

//Olhe como destructuring pode ajudar seu codigo:
let a = {
    especie: 'cachorro',
    som: 'auau'
}
//Antes:
function fazSom(objeto){
    let especie = objeto.especie || 'animal';
    let som = objeto.som;
    console.log('O ' + objeto.especie + ' faz ' + objeto.som);
}

//Depois 
let fazSom2 = ({ especie = 'animal', som }) => {
    console.log('O ' + especie + ' faz ' + som);
}

fazSom(a);
fazSom2(a);
