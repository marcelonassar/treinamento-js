//Podem servir para clonar arrays ou objetos
let someArray = [1, 2, 3, 4];

//let newArray = [...someArray];
//console.log(newArray);

let someObj = {nome: 'Marcelo', sobrenome: 'Nassar'};

//let newObj = {...someObj};
//console.log(newObj);

//Pode tambem adicionar elementos
let newArray = [...someArray, 5, 6];
console.log(newArray);

let newObj = {...someObj, idade: 22};
console.log(newObj);

//No merge de 2 objetos, o ultimo tem vantagem em um atributo que ambom possuem
let obj1 = {name: 'Fuu', x: 2};
let obj2 = {name: 'Zuu', y: 3};

let mergedObj = {...obj1, ...obj2};
console.log(mergedObj);