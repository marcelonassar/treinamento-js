//formato do objeto
let obj = {
    especie: 'cachorro',
    nome: 'toto',
    fazSom: () => {
        console.log('auau');
    }
}

//Acessando um metodo do objeto
obj.fazSom()

//Adicionando um atributo ao objeto
console.log(obj)

obj.raca = 'poodle';

console.log(obj)