let animais = [
    {nome: 'Fluff', especie: 'coelho'},
    {nome: 'toto', especie: 'cachorro'},
    {nome: 'hamilton', especie: 'cachorro'},
    {nome: 'ursula', especie: 'gato'},
    {nome: 'harold', especie: 'peixe'},
    {nome: 'jimmy', especie: 'peixe'}
];

//Queremos uma array com o nome dos animais

let nomes = animais.map((animal) => {
    return animal.nome;
})

/*
let nomes = [];
for (let i = 0; i < animais.length; i++) {
    nomes.push(animais[i].nome);
}*/
console.log(nomes);