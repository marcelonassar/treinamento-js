let animais = [
    {nome: 'Fluff', especie: 'coelho'},
    {nome: 'toto', especie: 'cachorro'},
    {nome: 'hamilton', especie: 'cachorro'},
    {nome: 'ursula', especie: 'gato'},
    {nome: 'harold', especie: 'peixe'},
    {nome: 'jimmy', especie: 'peixe'}
];

//Com for loop
/*
let cachorros = [];
for (let i = 0; i < animais.length; i++) {
    if(animais[i].especie === 'cachorro'){
        cachorros.push(animais[i]);
    }
}*/

let cachorros = animais.filter((animal) => {
    return animal.especie === 'cachorro';
})

console.log(cachorros);
