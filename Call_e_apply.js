//As funcoes rodam outra funcao num escopo que é passado como parametro

let myself = {nome: 'Marcelo', sobrenome: 'Nassar', idade: 10};

function sayMyName() {
    console.log(this.nome + ' ' + this.sobrenome);
}

sayMyName.call(myself)
//sayMyName.apply(myself)

//Qual a diferenca entre as duas
//call aceita varios parametros, enquanto apply acieta uma array como segundo parametro

function amIOlder() {
    for(let i = 0; i < arguments.length; i++){
        if(this.idade > arguments[i]){
            console.log('sim');
        }else{
            console.log('nao');
        }
    }
}

amIOlder.call(myself, 10, 5, 60);
amIOlder.apply(myself, [10, 5, 60]);
